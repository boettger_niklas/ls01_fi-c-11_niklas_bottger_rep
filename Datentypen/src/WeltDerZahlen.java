/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
       int anzahlSterne = 1000000000 ;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 365000000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      int alterTage = 6943 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      int gewichtKilogramm = 150000 ; 
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
      int flaecheGroessteLand = 17098242 ;
    
    // Wie groß ist das kleinste Land der Erde?
    
      double flaecheKleinsteLand = 0.44 ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner in berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tieres in kg: " + gewichtKilogramm);
    
    System.out.println("Fläche des größten Landes in km²: " + flaecheGroessteLand);
    
    System.out.println("Fläche des kleinsten Landes in km²: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
