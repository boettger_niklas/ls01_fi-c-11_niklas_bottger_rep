import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomatneu {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		float zuZahlenderBetrag; 
		float eingezahlterGesamtbetrag = 0; 
		while (true) {
			zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur, "Zu zahlender Betrag (EURO): ",
					"Anzahl der Tickets:");
			eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur,
					"Noch zu zahlen:  %.2f EURO. %n Bitte Geld einwerfen:", zuZahlenderBetrag);
			fahrkartenAusgeben();
			
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		}
	}

	
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				warte(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void warte(int millisekunden) throws InterruptedException {
		Thread.sleep(millisekunden);
	}

	public static void muenzAusgabe(String text1, String text2) {
		System.out.println(text1 + " " + text2);
	}

	public static void rueckgeldAusgeben(float eingezahlterGesamtbetrag, float zuZahlenderBetrag) {

		float rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		System.out.printf("Der Rückgabebetrag in Höhe von  %.2f EURO %n", rueckgabebetrag);
		System.out.println("wird in folgenden Münzen ausgezahlt:");
		if (rueckgabebetrag > 0.00f) {

			while (rueckgabebetrag >= 2.0)
			{
				muenzAusgabe("2", "EURO");
				int temp = Math.round((rueckgabebetrag -= 2.0) * 100);
				rueckgabebetrag = (float) temp / 100;

			}
			while (rueckgabebetrag >= 1.0)
			{
				muenzAusgabe("1", "EURO");
				int temp = Math.round((rueckgabebetrag -= 1.0) * 100);
				rueckgabebetrag = (float) temp / 100;
			}
			while (rueckgabebetrag >= 0.5)
			{
				muenzAusgabe("50", "CENT");
				int temp = Math.round((rueckgabebetrag -= 0.5) * 100);
				rueckgabebetrag = (float) temp / 100;
			}
			while (rueckgabebetrag >= 0.2)
			{
				muenzAusgabe("20", "CENT");
				int temp = Math.round((rueckgabebetrag -= 0.2) * 100);
				rueckgabebetrag = (float) temp / 100;
			}
			while (rueckgabebetrag >= 0.1) 
			{
				muenzAusgabe("10", "CENT");
				int temp = Math.round((rueckgabebetrag -= 0.1) * 100);
				rueckgabebetrag = (float) temp / 100;
			}
			while (rueckgabebetrag >= 0.05)
			{
				muenzAusgabe("5", "CENT");
				int temp = Math.round((rueckgabebetrag -= 0.05) * 100);
				rueckgabebetrag = (float) temp / 100;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static float fahrkartenbestellungErfassen(Scanner tastatur, String text, String text2) {
		boolean fehler = false;
		byte auswahl = 0;
		float preis = 0, zuZahlenderBetrag = 0;
		do {
			do {
				fehler = false;
				System.out.printf("\n\nWillkommen!\n" + "Bitte wählen sie ein der folgenden Fahrkartentariefe:\n"
						+ "Einzelfahrausweis Regeltarif AB [2,90 EUR] (1)\n"
						+ "Tageskarte Regltarif AB [8,60 EUR] (2)\n"
						+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n" + "Bezahlen (9)\n");
				auswahl = tastatur.nextByte();
				if (auswahl < 1 || (auswahl > 3 && auswahl != 9)) {
					System.out.println("Fehlerhafte Eingabe");
					fehler = true;
				}
			} while (fehler != false);
			switch (auswahl) {
			case 1:
				preis = 2.90f;
				break;
			case 2:
				preis = 8.60f;
				break;
			case 3:
				preis = 23.50f;
				break;
			case 9:
				break;
			}
			do {
				if (auswahl==9) {
					break;
				}
				fehler = false;
				System.out.println(text2);
				byte anzahl = tastatur.nextByte();
				if (anzahl > 10 || anzahl <= 0) {
					System.out.println("Es können maximal 10 Fahrkarten auf einmal gekauft werden!");
					anzahl = 1;
					fehler = true;
				}
				zuZahlenderBetrag+=preis * anzahl; 
				System.out.printf("Zwischensumme: %.2f EUR", zuZahlenderBetrag);

			} while (fehler != false);
		} while (auswahl != 9);
		return zuZahlenderBetrag;
	}

	public static float fahrkartenBezahlen(Scanner tastatur, String text, float zuZahlenderBetrag) {
		float eingeworfeneMünze;
		float eingezahlterGesamtbetrag = 0.00f;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf(Locale.US, text, zuZahlenderBetrag - eingezahlterGesamtbetrag);
			eingeworfeneMünze = tastatur.nextFloat();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

}