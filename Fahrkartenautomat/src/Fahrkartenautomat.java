﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       double rückgabebetrag;
       int anzahlTickets;

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextFloat();
       System.out.print("Anzahl der Tickets");
       anzahlTickets = tastatur.nextInt();
       
       
       if(zuZahlenderBetrag <= 0) {
    	   zuZahlenderBetrag = 3.5;
    	   System.out.println("Der zu zahlende Betrag wurde falsch eingegeben. Es wird ein Ticketpreis von 3,50€ berechnet.");
       }
       
       if(anzahlTickets != 1 && anzahlTickets != 2 && anzahlTickets != 3 && anzahlTickets != 4 && anzahlTickets != 5 && anzahlTickets != 6 && anzahlTickets != 7 && anzahlTickets != 8 && anzahlTickets != 9 && anzahlTickets != 10) {
    	   System.out.println("Die Anzahl der Tickets muss eine Zahl von 1 bis 10 sein. Es wird nur ein Ticket berechnet");
    	   anzahlTickets = 1;
       }
       
       zuZahlenderBetrag *= anzahlTickets;
      

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0f;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.print("Noch zu zahlen: ");
    	   System.out.printf("%.2f" , zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.println(" Euro");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.print("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f" , rückgabebetrag);
    	   System.out.print(" EURO ");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
        	  long temp = Math.round((rückgabebetrag -= 2.0)*100);
        	  rückgabebetrag = (float)temp/100;
	          // rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
        	  long temp = Math.round((rückgabebetrag -= 1.0)*100);
        	  rückgabebetrag = (float)temp/100;
	          // rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
        	  long temp = Math.round((rückgabebetrag -= 0.5)*100);
        	  rückgabebetrag = (float)temp/100;
	          // rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
        	  long temp = Math.round((rückgabebetrag -= 0.2)*100);
        	  rückgabebetrag = (float)temp/100;
 	          // rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
        	  long temp = Math.round((rückgabebetrag -= 0.1)*100);
        	  rückgabebetrag = (float)temp/100;
	          // rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
        	  long temp = Math.round((rückgabebetrag -= 0.05)*100);
        	  rückgabebetrag = (float)temp/100;
 	          // rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       tastatur.close();
    }
}