import java.util.Scanner;

public class FahrkartenautomatM {

	public static void main(String[] args) {
		float zuZahlenderBetrag; 
	    float eingezahlterGesamtbetrag;
	    // float eingeworfeneMünze;
	    float rückgabebetrag;
	    int anzahlTickets;
	    
	    zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    fahrkartenAusgeben();
	    rückgeldAusgeben = rückgabebetrag();

	}
	
	public static double fahrkartenbestellungErfassen(String[] args){
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Ticketpreis (EURO): ");
	    float zuZahlenderBetrag = tastatur.nextFloat();
	    System.out.print("Anzahl der Tickets");
	    int anzahlTickets = tastatur.nextInt();
	    // zuZahlenderBetrag *= anzahlTickets;
	    // return zuZahlenderBetrag;
	    
	    return zuZahlenderBetrag *= anzahlTickets;
		
	}
	
	public static float fahrkartenBezahlen(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		float eingezahlterGesamtbetrag = 0.0f;
		while(eingezahlterGesamtbetrag < zuZahlen) {
			
			System.out.println("Noch zu zahlen: " + (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Einwurf");
			float eingeworfenerBetrag eingeworfeneMünze = tastatur.nextFloat();
			eingezahlterGesamtbetrag += eingeworfenerBetrag;
		}
	}
	
	public static float fahrkartenAusgeben(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static void rückgeldAusgeben(float rückgabebetrag) {
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.print("Der Rückgabebetrag in Höhe von ");
	    	   System.out.printf("%.2f" , rückgabebetrag);
	    	   System.out.print(" EURO ");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
	        	  int temp = Math.round((rückgabebetrag -= 2.0)*100);
	        	  rückgabebetrag = (float)temp/100;
		          // rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
	        	  int temp = Math.round((rückgabebetrag -= 1.0)*100);
	        	  rückgabebetrag = (float)temp/100;
		          // rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
	        	  int temp = Math.round((rückgabebetrag -= 0.5)*100);
	        	  rückgabebetrag = (float)temp/100;
		          // rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	        	  int temp = Math.round((rückgabebetrag -= 0.2)*100);
	        	  rückgabebetrag = (float)temp/100;
	 	          // rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
	        	  int temp = Math.round((rückgabebetrag -= 0.1)*100);
	        	  rückgabebetrag = (float)temp/100;
		          // rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	        	  int temp = Math.round((rückgabebetrag -= 0.05)*100);
	        	  rückgabebetrag = (float)temp/100;
	 	          // rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	}

}
