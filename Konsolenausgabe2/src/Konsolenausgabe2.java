
public class Konsolenausgabe2 {
	public static void main(String[] args) {
		String s = "**";
		String d = "*";
		
		System.out.printf("%5s\n", s);
		System.out.print("*");
		System.out.printf("%7s\n", d);
		System.out.print("*");
		System.out.printf("%7s\n", d);
		System.out.println("   **");
		System.out.println("\n");
		
		System.out.printf("0!   =                   =   1\n");
		System.out.printf("1!   = 1                 =   1\n");
		System.out.printf("2!   = 1 * 2             =   2\n");
		System.out.printf("3!   = 1 * 2 * 3         =   6\n");
		System.out.printf("4!   = 1 * 2 * 3 * 4     =   24\n");
		System.out.printf("5!   = 1 * 2 * 3 * 4 * 5 =   120\n");
		System.out.println("\n");
		
		String Fahrenheit = "Fahrenheit", Celsius = "Celsius", linie = "------------------------";
		String f = "-20", f2 = "-10", f3 = "+0", f4 = "+20", f5 = "+30";
		float c = -28.8889f, c2 = -23.3333f, c3 = -17.7778f, c4 = -6.6667f, c5 = -1.1111f;
		
		System.out.printf("%s  |   ", Fahrenheit);
		System.out.printf("%s\n", Celsius);
		System.out.print(linie + "\n");
		System.out.printf("%s         |    %.2f%n", f, c);
		System.out.printf("%s         |    %.2f\n", f2, c2);
		System.out.printf("%s          |    %.2f\n", f3, c3);
		System.out.printf("%s         |    %.2f\n", f4, c4);
		System.out.printf("%s         |    %.2f\n", f5, c5);
	}
}
