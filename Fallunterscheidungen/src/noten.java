import java.util.Scanner;

public class noten {
	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		int a;
		System.out.print("Bitte Note (1-6) eingeben: ");
	    a = myScanner.nextInt();
	    
	    myScanner.close();
	    

	if(a==1){
		System.out.println("1 = Sehr gut");
	}
	if(a==2){
		System.out.println("2 = Gut");
	}
	if(a==3){
		System.out.println("3 = Befriedigend");
	}
	if(a==4){
		System.out.println("4 = Ausreichend");
	}
	if(a==5){
		System.out.println("5 = Mangelhaft");
	}
	if(a==6){
		System.out.println("6 = Ungenügend");
	}
	if(a!=1 && a!=2 && a!=3 && a!=4 && a!=5 && a!=6){
		System.out.println("Fehlerhafte Eingabe, bitte ganze Zahl von 1 bis 6 eingeben.");
	}
}
}
