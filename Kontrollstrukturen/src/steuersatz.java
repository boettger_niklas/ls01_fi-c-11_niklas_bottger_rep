import java.util.Scanner;

public class steuersatz {
	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		double nettowert;
		char satz;
		System.out.print("Nettowert eingeben: ");
	    nettowert = myScanner.nextDouble();
	    System.out.print("ermäßigter oder voller steuersatz? 'j' für ermäßigt und 'n' für voll ");
	    satz = myScanner.next().charAt(0);
	    
	    myScanner.close();
	    

	if(satz == 'j'){

		System.out.println("ermäßigter Steuersatz");
		System.out.println("Nettowert: " +nettowert);
		double bruttowert = nettowert+nettowert/100*7;
		System.out.println("Bruttowert: " +bruttowert);
	}
	if(satz == 'n'){
		
		System.out.println("voller Steuersatz");
		System.out.println("Nettowert: " +nettowert);
		double bruttowert = nettowert+nettowert/100*19;
		System.out.println("Bruttowert: " +bruttowert);
	}
	if(satz != 'n' && satz != 'j') {
		System.out.println("fehlerhafte eingabe");
	}
	}
}
