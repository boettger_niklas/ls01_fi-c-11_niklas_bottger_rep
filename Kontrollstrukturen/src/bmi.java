import java.util.Scanner;

public class bmi {
	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		double gewicht;
		double grosse;
		char geschlecht;
		System.out.println("BMI Rechner");
		System.out.print("Bitte Gewicht in kg eingeben: ");
	    gewicht = myScanner.nextDouble();
	    System.out.println("Bitte Körpergröße in cm eingeben: ");
	    grosse = myScanner.nextDouble();
	    System.out.println("Bitte Geschlecht eingeben (m/w): ");
	    geschlecht = myScanner.next().charAt(0);
	    
	    myScanner.close();
	    
	    double bmi = (double)gewicht/(Math.pow(grosse, 2)/10000);
	    

	if(geschlecht == 'm' && bmi < 20){

		System.out.println("Ihr BMI ist: "+bmi);
		System.out.println("Sie sind also untergewichtig.");
	}
		
	if(geschlecht == 'm' && bmi > 20 && bmi < 25){

		System.out.println("Ihr BMI ist: "+bmi);
		System.out.println("Sie sind also normalgewichtig.");
	}
	
	if(geschlecht == 'm' && bmi > 25){

		System.out.println("Ihr BMI ist: "+bmi);
		System.out.println("Sie sind also übergewichtig.");
	}
	
	if(geschlecht == 'w' && bmi < 19){

		System.out.println("Ihr BMI ist: "+bmi);
		System.out.println("Sie sind also untergewichtig.");
	}
		
	if(geschlecht == 'w' && bmi > 19 && bmi < 24){

		System.out.println("Ihr BMI ist: "+bmi);
		System.out.println("Sie sind also normalgewichtig.");
	}
	
	if(geschlecht == 'w' && bmi > 24){

		System.out.println("Ihr BMI ist: "+bmi);
		System.out.println("Sie sind also übergewichtig.");
	}
	
	if(geschlecht != 'm' && geschlecht != 'w') {
		System.out.println("fehlerhafte eingabe");
	}
	}
}
