import java.util.Scanner;

public class GGT {
  
  public static void main(String[] args) {
    int a, b, zahl1, zahl2;
    
    Scanner sc = new Scanner(System.in);
        
    System.out.print("Bitte erste Zahl eingeben: ");
    zahl1 = a = sc.nextInt();
    
    System.out.print("Bitte zweite Zahl eingeben: ");
    zahl2 = b = sc.nextInt();
    
    while (a > 0 && b > 0)
    {
      if (a > b)
        a = a - b;
      else
        b = b - a;
    }
    if (b == 0)
      System.out.printf("Der GGT von %d und %d ist %d", zahl1, zahl2, a);
    else
      System.out.printf("Der GGT von %d und %d ist %d", zahl1, zahl2, b);
  } // end of main

} // end of class GGT

