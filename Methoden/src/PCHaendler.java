import java.util.Scanner;

public class PCHaendler {
//
	public static void main(String[] args) {
		String artikel;
		int anzahl;
		double nettopreis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;
		
		Scanner myScanner = new Scanner(System.in);
		artikel = liesString(myScanner, "Was möchten Sie bestellen?");
		anzahl = liesInt(myScanner, "Geben Sie die Anzahl ein:");
		nettopreis = liesDouble(myScanner, "Geben Sie den Nettopreis ein:");
		mwst = liesDouble(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		myScanner.close();

	}
	
	// Benutzereingaben lesen
	 public static String liesString(Scanner myScanner, String text) {
		 
		 System.out.println(text);
		 String artikel = myScanner.next();
		 return artikel;
	 }
	 
	 public static int liesInt(Scanner myScanner, String text) {
		 
		 System.out.println(text);
		 int anzahl = myScanner.nextInt();
		 return anzahl;
	 }

	 public static double liesDouble(Scanner myScanner, String text) {
		 
		 System.out.println(text);
		 double doubl = myScanner.nextDouble();
		 return doubl;

	 }
	 
	// Verarbeiten
	 public static double berechneGesamtnettopreis(int anzahl, double
			 nettopreis) {
		 
		 double nettogesamtpreis = anzahl * nettopreis;
		 return nettogesamtpreis;
	 }
	 
	 public static double berechneGesamtbruttopreis(double nettogesamtpreis,
			 double mwst) {
		 
		 double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		 return bruttogesamtpreis;
	 }
	 
	// Ausgeben
	 public static void rechnungausgeben(String artikel,
			 int anzahl, double nettogesamtpreis, double bruttogesamtpreis, 
			 double mwst) {
		 
		 System.out.println("\tRechnung");
		 System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		 System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		 

	 }
}