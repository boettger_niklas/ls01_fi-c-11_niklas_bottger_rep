import java.util.Scanner;

public class MittelwertMitMethoden {
	
	public static void main (String[] args) {
		double summe = 0;
		double mittelwert;
		int anzahlWerte;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		anzahlWerte = eingabeAnzahl(myScanner, "Anzahl der einzugebenden Zahlen");
		
		for(int i = 0; i < anzahlWerte; i++) {
			summe += eingabe(myScanner, "Geben Sie einen neuen Wert ein: ");
		}
		
		mittelwert = mittelwertBerechnung(summe, anzahlWerte);
		
		ausgabe(mittelwert);
		myScanner.close();
	}
	
	public static void programmhinweis(String text) {
		System.out.println(text);
		
	}
	
	public static int eingabeAnzahl(Scanner ms, String text) {
		System.out.println(text);
		int eingabe = ms.nextInt();
		return eingabe;
	}
	
	public static double eingabe(Scanner myScanner, String text) {
		
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	
	public static double mittelwertBerechnung(double zahl, int anzahl) {
		
		double m = zahl/ anzahl;
		return m;
	}
	
	public static void ausgabe(double m) {
		System.out.println("Mittelwert: " + m);
		
	}

}
